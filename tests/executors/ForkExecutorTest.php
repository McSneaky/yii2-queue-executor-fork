<?php

namespace tests\executors;

use tests\executors\jobs\SuccessJob;
use tests\executors\jobs\RejectJob;
use Exception;

/**
* 
*/
class ForkExecutorTest extends TestCase
{
    /**
     * Gets the executor.
     *
     * @return     \yii\queue\executors\fork\Executor     The executor.
     */
    public function getExecutor()
    {
        return new \yii\queue\executors\fork\Executor([
            'queue' => $this->getQueue()
        ]);
    }


    public function testSuccessfulJob()
    {
        $executor = $this->getExecutor();

        $job = new SuccessJob([
            'return' => 'test'
        ]);

        $message = $this->getQueue()->getSerializer()->serialize($job);

        $executor->handleMessage($message)->then(function ($result) use (&$actual) {
            $actual = $result;
        });
        $executor->getManager()->wait();

        $this->assertEquals('test', $actual);

    }

    public function testRejectedJob()
    {
        $executor = $this->getExecutor();

        $job = new RejectJob([
            'message' => 'test'
        ]);

        $message = $this->getQueue()->getSerializer()->serialize($job);

        $executor->handleMessage($message)->then(
            function ($result) {}, 
            function ($e) use (&$actual) {
                $actual = $e;
            }
        );

        $executor->getManager()->wait();

        $this->assertInstanceOf(Exception::class, $actual);
        $this->assertEquals('test', $actual->getMessage());
    }
}