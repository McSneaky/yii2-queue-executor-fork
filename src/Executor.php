<?php

namespace yii\queue\executors\fork;

use Kraken\Promise\Promise;
use Spork\ProcessManager;
use Spork\Fork;
use \Exception;

/**
 */
class Executor extends \yii\queue\executors\Executor
{
    private $_manager;

    /**
     * Gets the manager.
     *
     * @return     ProcessManager  The spork proccess manager.
     */
    public function getManager()
    {
        if (!$this->_manager) {
            $this->_manager = new ProcessManager();
        }
        return $this->_manager;
    }

    /**
     * @inheritdoc
     */
    public function handleMessage($message, $id = null, $ttr = null, $attempt = null)
    {
        return new Promise(function ($resolve, $reject) use (&$message) {
            $serializer = $this->getQueue()->getSerializer();
            $this->getManager()->fork(function () use ($message, $serializer) {
                $job = $serializer->unserialize($message);

                return $job->execute();
            })->then(function (Fork $fork) use (&$resolve) {
                call_user_func($resolve, $fork->getResult());
            }, function (Fork $fork) use (&$reject) {
                /**
                 * @var        \Spork\Util\Error
                 */
                $error = $fork->getError();
                $className = $error->getClass();

                /**
                 * @var \Exception
                 */
                $exception = new $className($error->getMessage(), $error->getCode());
                call_user_func($reject, $exception);
            });
        });
    }
}
